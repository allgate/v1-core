// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "../../contracts/oft/v2/OFTV2.sol";
import "../../interfaces/IOFTMigratable.sol";

contract OFTMigratable is IOFTMigratable, OFTV2 {
    address public migration;

    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        address _deployer,
        address _lzEndpoint
    ) OFTV2(_name, _symbol, _decimals, _lzEndpoint, _deployer) {
        migration = _msgSender();
    }

    function mintTo(address _to, uint256 _amount) external override {
        require(_msgSender() == migration);
        _mint(_to, (_amount * 10 ** decimals()));
    }
}
