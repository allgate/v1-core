// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

//import "./Migration.sol";
//import "../../interfaces/IMigrationValidator.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract MigrationsFactory is ReentrancyGuard {
//
//    event Created(address _address, address _deployer);
//
//    mapping(address => address) public deployers; // Migrated Token -> Its owner (deployer)
//    address public lzEndpoint;
//    address public owner;
//
//    constructor (
//        address _lzEndpoint
//    ) {
//        require(_lzEndpoint != address(0));
//        owner = msg.sender;
//        lzEndpoint = _lzEndpoint;
//    }
//
//    function create(address _migratedToken) external nonReentrant {
//        require(_migratedToken != address(0));
//        require(deployers[_migratedToken] == msg.sender);
//
//        address migrationAddress = address(new Migration(_migratedToken, msg.sender, lzEndpoint));
//
//        emit Created(migrationAddress, msg.sender);
//    }
//
//    function setDeployer(address _deployer, address _migratedToken) external {
//        require(msg.sender == owner);
//        deployers[_migratedToken] = _deployer;
//    }
}
