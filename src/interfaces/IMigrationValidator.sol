// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "./IOFTMigratable.sol";

interface IMigrationValidator {
    function validate(IOFTMigratable _oft, address _migrator) external returns (bool);
}
