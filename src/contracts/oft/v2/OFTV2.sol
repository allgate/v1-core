// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./fee/OFTWithFee.sol";

contract OFTV2 is OFTWithFee {
    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        address _lzEndpoint,
        address _deployer
    ) OFTWithFee(_name, _symbol, _decimals, _lzEndpoint) {
        _transferOwnership(_deployer);
        feeOwner = owner();
    }
}
