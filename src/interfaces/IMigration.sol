// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface IMigration {
    function setRollback(bool _isRollbackEnabled) external;
}
