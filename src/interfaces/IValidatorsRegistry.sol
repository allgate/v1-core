// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "./IMigrationValidator.sol";

interface IValidatorsRegistry {
    function get(bytes4 _strategyKey) external returns (IMigrationValidator);
}
