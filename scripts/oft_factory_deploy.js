const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'ftm_testnet';
const networkToDeployParamsMap = {
    goerli: {
        lzEndpoint: '0xbfD2135BFfbb0B5378b56643c2Df8a87552Bfa23',
    },
    bsc_testnet: {
        lzEndpoint: '0x6Fcb97553D41516Cb228ac03FdC8B9a0a9df04A1',
    },
    avalanche_testnet: {
        lzEndpoint: '0x93f54D755A063cE7bB9e6Ac47Eccc8e33411d706',
    },
    polygon_testnet: {
        lzEndpoint: '0xf69186dfBa60DdB133E91E9A4B5673624293d8F8',
    },
    // arbitrum_testnet: {
    //     lzEndpoint: '0x6aB5Ae6822647046626e83ee6dB8187151E1d5ab',
    // },
    // optimism_testnet: {
    //     lzEndpoint: '',
    // },
    ftm_testnet: {
        lzEndpoint: '0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf',
    },
    // moonbeam_testnet: {
    //     lzEndpoint: '',
    // },
    zksync_testnet: {
        lzEndpoint: '0x852a4599217e76aa725f0ada8bf832a1f57a8a91',
    },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    console.log(`Deploying OFTFactory for !!! ${network} !!!`);
    const contractFactory = await hre.ethers.getContractFactory("OFTFactory");
    const contract = await contractFactory.deploy(networkParams.lzEndpoint);
    await contract.deployed();
    console.log("OFTFactory deployed to:", contract.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: contract.address,
                        constructorArguments: [
                            networkParams.lzEndpoint,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
