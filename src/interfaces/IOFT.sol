// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface IOFT {
    function mintTo(address _to, uint256 _amount) external;
}
