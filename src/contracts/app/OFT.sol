// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "../../contracts/oft/v2/OFTV2.sol";

contract OFT is OFTV2 {
    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        address _lzEndpoint,
        address _deployer,
        uint256 _totalSupply
    ) OFTV2(_name, _symbol, _decimals, _lzEndpoint, _deployer) {
        if (_totalSupply > 0) {
            _mint(_deployer, (_totalSupply * 10 ** _decimals));
        }
    }

    // TODO: (Must) updateLzConfig() i updateLzEndpoint() onlyOwner, from OFTFactory
}
