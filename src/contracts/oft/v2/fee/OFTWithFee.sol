// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./BaseOFTWithFee.sol";

contract OFTWithFee is BaseOFTWithFee, ERC20 {

    uint internal ld2sdRate;
    uint8 internal _decimals;

    constructor(string memory _name, string memory _symbol, uint8 _tokenDecimals, address _lzEndpoint) ERC20(_name, _symbol) BaseOFTWithFee(_tokenDecimals, _lzEndpoint) {
        _decimals = _tokenDecimals;
        ld2sdRate = 10 ** (_decimals - sharedDecimals);
    }

    function circulatingSupply() public view virtual override returns (uint) {
        return totalSupply();
    }

    function token() public view virtual override returns (address) {
        return address(this);
    }

    function _debitFrom(address _from, uint16, bytes32, uint _amount) internal virtual override returns (uint) {
        address spender = _msgSender();
        if (_from != spender) _spendAllowance(_from, spender, _amount);
        _burn(_from, _amount);
        return _amount;
    }

    function _creditTo(uint16, address _toAddress, uint _amount) internal virtual override returns (uint) {
        _mint(_toAddress, _amount);
        return _amount;
    }

    function _transferFrom(address _from, address _to, uint _amount) internal virtual override returns (uint) {
        address spender = _msgSender();
        if (_from != address(this) && _from != spender) _spendAllowance(_from, spender, _amount);
        _transfer(_from, _to, _amount);
        return _amount;
    }

    function _ld2sdRate() internal view virtual override returns (uint) {
        return ld2sdRate;
    }

    function decimals() public override view returns (uint8) {
        return _decimals;
    }
}
