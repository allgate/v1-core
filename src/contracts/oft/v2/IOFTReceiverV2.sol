// SPDX-License-Identifier: MIT

pragma solidity >=0.5.0;

interface IOFTReceiverV2 {
    function onOFTReceived(uint16 _srcChainId, bytes calldata _srcAddress, uint64 _nonce, bytes32 _from, uint _amount, bytes calldata _payload) external;
}
