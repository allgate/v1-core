const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const exampleNft = await hre.ethers.getContractAt(
        ["function burn(uint256 tokenId) external"],
        "0xb4b06AF3D5f8b5024B32b21Fb99178495fa563Bf", // collection
        (await hre.ethers.getSigners())[0],
    );

    // tokenId
    await exampleNft.burn(1);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });