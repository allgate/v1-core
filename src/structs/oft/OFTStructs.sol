// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";

struct MigrateOFTParams {
    IERC20Metadata migratedToken;
    uint256 from;
    uint256 to;
    bool isBiDirectional;
    bytes32 allowlistMerkleRoot;
}
