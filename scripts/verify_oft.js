const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        contract: "src/contracts/app/OFT.sol:OFT",
                        address: "0x940778F642Da4FA1a96C90F925D1d83Ae1e74cc2",
                        constructorArguments: [
                            "Fantom OFT",
                            "fOFT",
                            18,
                            "0x7dcAD72640F835B0FA36EFD3D6d3ec902C7E5acf",
                            "0x2fAAAa87963fdE26B42FB5CedB35a502d3ee09B3",
                            25000,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 1000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
