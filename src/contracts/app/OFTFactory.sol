// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "./OFT.sol";

contract OFTFactory {
    event OFTDeployed(address indexed _oft, address indexed _deployer);

    address public owner;
    address public lzEndpoint;

    constructor (address _lzEndpoint) {
        require(_lzEndpoint != address(0));
        lzEndpoint = _lzEndpoint;
        owner = msg.sender;
    }

    function create(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        uint256 _totalSupply
    ) external returns (address oft) {
        require(_decimals > 0 && _decimals <= 18);
        require(bytes(_name).length > 0);
        require(bytes(_symbol).length > 0);

        oft = address(new OFT(_name, _symbol, _decimals, lzEndpoint, msg.sender, _totalSupply));
        emit OFTDeployed(oft, msg.sender);
    }

    function setLzEndpoint(address _lzEndpoint) external {
        require(msg.sender == owner);
        require(_lzEndpoint != address(0));
        lzEndpoint = _lzEndpoint;
    }
}
