const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const TokenMock = await hre.ethers.getContractFactory("TokenMock");
    const token = await TokenMock.deploy();
    await token.deployed();
    console.log("TokenMock deployed to:", token.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: token.address,
                        contract: "src/contracts/TokenMock.sol:TokenMock",
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });