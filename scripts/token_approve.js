const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");
const {ethers} = require("ethers");

async function main() {
    const exampleNft = await hre.ethers.getContractAt(
        ["function approve(address spender, uint256 amount) public"],
        "0x2b2CdEBE3c93f487ACDA9BF05f39206d53b83365", // token
        (await hre.ethers.getSigners())[0],
    );
    const gatewayAddress = '0x7343B5004f9dD5533FD62970dD893561FA9fB5c5'; // ftm gateway

    // ONFTGateway, tokenId
    await exampleNft.approve(gatewayAddress, ethers.utils.parseUnits((1000).toString(), 18));
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
