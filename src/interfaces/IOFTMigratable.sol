// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface IOFTMigratable is IERC20 {
    function mintTo(address _to, uint256 _amount) external;
}
