// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/interfaces/IERC20Metadata.sol";
import "./OFTMigratable.sol";
import "../../interfaces/IOFTMigratable.sol";
import "../../interfaces/IMigrationValidator.sol";
import "../../interfaces/IValidatorsRegistry.sol";
//import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";

contract Migration is ReentrancyGuard {
    event OFTDeployed(address _oft);
    event Migrated(address _owner, uint256 _amount);

    IERC20Metadata public migratedToken;
    IOFTMigratable public oft;
    address public migrationsFactory;
    address public deployer;
    address public lzEndpoint;
    IMigrationValidator public validator;
    IValidatorsRegistry internal _validatorsRegistry;

    constructor(
        address _migratedToken,
        address _deployer,
        address _lzEndpoint
    ) {
        migrationsFactory = msg.sender;
        migratedToken = IERC20Metadata(_migratedToken);
        deployer = _deployer;
        lzEndpoint = _lzEndpoint;
        _validatorsRegistry = IValidatorsRegistry(address(this)); // TODO (Must) Set to real contract address
    }

    function deployOFT(string memory _name, string memory _symbol) external {
        require(msg.sender == deployer);
        oft = new OFTMigratable(_name, _symbol, migratedToken.decimals(), msg.sender, lzEndpoint);
        emit OFTDeployed(address(oft));
    }

    // bytes32[] memory _merkleProof
    function migrate() public nonReentrant {
        address holder = msg.sender;

        if (address(validator) != address(0)) {
            validator.validate(oft, holder);
        }

        // Migrated Token -> OFT
        uint256 migratedTokenBalance = migratedToken.balanceOf(holder);
        require(migratedTokenBalance > 0);
        migratedToken.transferFrom(holder, address(this), migratedTokenBalance);
        oft.mintTo(holder, migratedTokenBalance);
    }

    // TODO: (Must) This can be rug pull - implement predefined ValidatorsRegistry with Validators mapping added by Owner (Allgate)
    function setValidator(bytes4 _strategyKey) external {
        require(msg.sender == deployer);
        require(address(oft) == address(0) || oft.totalSupply() == 0);
        validator = _validatorsRegistry.get(_strategyKey);
    }
}
